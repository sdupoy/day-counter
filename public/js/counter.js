function pad(n) {
    return n.toString().padStart(2, '0');
}

function displayDelta(delta) {
    var deltaString = pad(delta.days) + ":" + pad(delta.hours) + ":" + pad(delta.minutes) + ":" + pad(delta.seconds);
    document.getElementById("delta").innerHTML = deltaString;
    document.getElementById("failure").style.display = "none";
    document.getElementById("finished").style.display = "none";
    document.getElementById("delta").style.display = "block";
}

function displayFinished() {
    document.getElementById("delta").style.display = "none";
    document.getElementById("failure").style.display = "none";
    document.getElementById("finished").style.display = "block";
}

function displayFailure() {
    document.getElementById("delta").style.display = "none";
    document.getElementById("finished").style.display = "none";
    document.getElementById("failure").style.display = "block";
}

function isFinished(delta) {
    return
        delta.days === 0
        && delta.hours === 0
        && delta.minutes === 0
        && delta.seconds === 0;
}

function handleSuccess(delta) {
    if (isFinished(delta)) {
        displayFinished();
    }
    else {
        displayDelta(delta);
        setTimeout(check, 1000);
    }
}

function check() {
    var request = new XMLHttpRequest();
    request.open("GET", "/calculate", true);

    request.onload = function() {
        // assume success
        if (request.status == 200) {
            return handleSuccess(JSON.parse(request.responseText));
        }
        return displayFailure();
    };
    request.onerror = displayFailure;
    request.send();
}

check();
