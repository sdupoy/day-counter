// dependencies
var http = require('http');
var path = require('path');
var express = require('express');
var sass = require('node-sass-middleware');

// local dependencies
var calculator = require("./calculator.js");

// server setup
var host = '0.0.0.0';
var port = 8091;

// express setup
var app = express();
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(
    sass({
        src: path.join(__dirname, '/scss'),
        dest: path.join(__dirname, '/public/style'),
        prefix: '/style',
        debug: true,
    })
);
app.use(express.static(path.join(__dirname, 'public')));

// routes
app.get('/', function(req, res) { res.render('home'); });
app.get("/calculate", calculator.calculate);

// http server. ssl termination is at nginx level.
var server = http.Server(app);
server.listen(port, host, function() {
    console.log('server started listening on ' + host + ':' + port);
});

