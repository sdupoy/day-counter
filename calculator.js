var holidays = [
    new Date("2018-10-29"),
    new Date("2018-10-30"),
    new Date("2018-10-31"),
    new Date("2018-11-01"),
    new Date("2018-11-02"),
];

// could be 2nd
var end = new Date("2018-11-09T17:00:00Z");
var startTimeInSeconds = 9 * 3600;
var endTimeInSeconds = 17 * 3600;

function isSameDate(date1, date2) {
    return date1.toString().substring(0, 10) === date2.toString().substring(0, 10);
}

function isWeekDay(date) {
    var day = date.getDay();
    return day != 0 && day != 6;
}

function isHoliday(date) {
    var dateIsHoliday = false;
    holidays.forEach(function(holiday) {
        if (isSameDate(date, holiday)) {
            dateIsHoliday = true;
        }
    });
    return dateIsHoliday;
}

function calculate(now)
{
    var result = {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
    }

    // check we're not past the last date
    if (now >= end) {
        return result;
    }

    // not past the last date so work back working out the delta
    var date = end;
    while (true) {
        // if it's not the same date then increment if it's not a weekend and continue
        if (!isSameDate(now, date)) {
            if (!isHoliday(date) && isWeekDay(date)) {
                result.days++;
            }
            date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1);
            continue;
        }

        // it's the last date so check if it's a weekend or holiday
        if (isHoliday(date) || !isWeekDay(date)) {
            break;
        }

        // now work out the time in seconds and check it's within the working hours
        var timeInSeconds = now.getSeconds() + now.getMinutes() * 60 + now.getHours() * 3600;
        if (timeInSeconds >= endTimeInSeconds) {
            // past end time
            break;
        }
        if (timeInSeconds <= startTimeInSeconds) {
            // still got the whole day to go
            result.days++;
            break;
        }

        // we're in the day so work out the remaining seconds for the day and div/mod out the parts
        var remainingSeconds = endTimeInSeconds - timeInSeconds;
        result.hours = Math.floor(remainingSeconds / 3600);
        result.minutes = Math.floor((remainingSeconds % 3600) / 60);
        result.seconds = remainingSeconds % 60;
        break;
    }

    return result;
}

module.exports.calculate = function(req, res) {
    res.send(calculate(new Date()));
}
